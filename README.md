# README #

PySick is a Python-based driver for SICK laser scanners/lidars.

Written for Python 3.5+

##Basic instructions##
Put the pysick folder somewhere your Python can find it. Include pysick.PysickLidar and/or pysick.PysickPlot as you see fit.

##Step-by-step guide##
This is how you can easily run the example files:

**Installation**

- Clone this repo: `git clone https://bitbucket.org/aauvap/pysick.git`
- Go to the repository folder: `cd pysick`
- Install [miniconda](http://conda.pydata.org/miniconda.html)
- Make a new conda environment: `conda env create -f pysick.yml`

**Running the examples**

- Activate the conda environment (if not active already): `source activate pysick` (Mac/Linux) or `activate pysick` (Windows)
- Go to the examples folder: `cd examples`
- Run the recorder: `python recorder.py` or viewer: `python viewer.py`

Access the help by running `python viewer.py -h`

## Dependencies ##

The driver (pysick.PysickLidar) requires Numpy. If you wish to also use the plotting functionality, the following dependencies apply, Matplotlib is also required.

These can easily be loaded using [conda](http://conda.pydata.org/miniconda.html):

```
#!bash

conda env create -f pysick.yml
```