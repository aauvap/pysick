import re
import datetime
import sys
import os
import argparse
import threading
import types
from pprint import pprint

# Allow include
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))
import pysick.PysickLidar as Pysick
from kbhit import KBHit
    
def keypressHandler(scanSettings, outfile):
    if scanSettings['keyboard'].kbhit():
        c = scanSettings['keyboard'].getch()
        if ord(c) == 27: # ESC
            scanSettings['run'] = False
        elif c == ' ':
            scanSettings['truck'] = not scanSettings['truck']
            if scanSettings['truck']:
                print("TruckStart")
                outfile.write("TruckStart\n")
            else:
                print("TruckEnd")
                outfile.write("TruckEnd\n")


def main(args):
    kb = KBHit() # Ready to receive keyboard input

    sensors = []
    for a in args.addresses:
        addr = a.split(":")
        addr = (addr[0], int(addr[1]))
        sensor = types.SimpleNamespace()
        sensor.sensor = Pysick.Lidar(addr)
        sensor.lastScanCount = -1
        sensors.append(sensor)
        sensors[-1].sensor.initialize(ScanFrequency = 25,
                               AngularResolution = 0.1667,
                               StartAngle = 5,
                               StopAngle = 175,
                               verbose = True)
    currentMinute = int(datetime.datetime.now().strftime("%M"))
    outfile = open(datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S") +
                   '.dat', 'w')

    scanSettings = {'run': True, 'numScans': 0, 'keyboard': kb, 'truck': False}

    print("Recording...")
    
    while scanSettings['run']:
        for sensor in sensors:
            data = sensor.sensor.singleScan()
            parsedData = Pysick.ScanData(data)
            if parsedData.ScanCounter == sensor.lastScanCount:
                # We just received a scans we have seen before. Ignore it.
                continue
            keypressHandler(scanSettings, outfile)
            outfile.write("{}\n".format(data))
            sensor.lastScanCount = parsedData.ScanCounter
            scanSettings['numScans'] += 1
        if int(datetime.datetime.now().strftime("%M")) == currentMinute+10:
            currentMinute = int(datetime.datetime.now().strftime("%M"))
            outfile.close()
            outfile = open(datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S") +
                   '.dat', 'w')

    for sensor in sensors:
        sensor.sensor.close()
    outfile.close()

    print("Recorded {} scans from {} sensors.".format(scanSettings['numScans'],
                                                      len(sensors)))
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Visualize lidar data either live or from a file.')
    parser.add_argument('addresses',
                        nargs = '*',
                        metavar = '192.168.0.1:2112',
                        default = ['192.168.0.1:2112'],
                        type = str,
                        help = 'The IP-address(es) of the sensor(s).')
    args = parser.parse_args()
    main(args)
