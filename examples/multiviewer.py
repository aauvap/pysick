import argparse
import warnings
import math
import os
import sys
import inspect
from collections import Counter
import datetime

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# Allow include
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))
import pysick.PysickLidar as Pysick
from pysick.PysickPlot import Plot
    
def keypressHandler(event, runControl):
    if event.key == 'escape':
        runControl['run'] = False
    if event.key == ' ':
        runControl['pause'] = not runControl['pause']
    if event.key == 'right' and runControl['pause']:
        runControl['step'] = True
    if event.key == 'left' and runControl['pause']:
        runControl['step'] = True
        runControl['nextLine'] -= 2
    if event.key in ['1','2','3','4','5','6','7','8','9','0']:
        if runControl['goto'] == None:
            runControl['goto'] = int(event.key)
        else:
            runControl['goto'] *= 10
            runControl['goto'] += int(event.key)
    if ((event.key == 'g' or event.key == 'enter') and
            runControl['goto'] != None):
        runControl['step'] = True
        runControl['nextLine'] = runControl['goto']
        runControl['goto'] = None

def closeWindowHandler(event, runControl):
    # Lambdas disallow assignment, so we'll have a proper function doing this.
    runControl['run'] = False

def drawVisualAids(fig, axisRange):
    ax = fig.add_subplot(111)
    # ax.plot([axisRange[0], axisRange[1]],
    #         [2500, 2500],
    #         '-', color = '0.75', lw = 2, zorder = 1)
    # ax.plot([axisRange[0], axisRange[1]],
    #         [6000, 6000],
    #         '--', color = '0.75', lw = 1, zorder = 1)
    # ax.plot([axisRange[0], axisRange[1]],
    #         [9500, 9500],
    #         '-', color = '0.75', lw = 2, zorder = 1)
    ax.add_patch(patches.Rectangle((-250, -500), 500, 500,
                                   facecolor="0.75", zorder = 1))
    startCoord = (0,0)
    startOffset = 500
    length = 800
    for angle in [10, 50, 90, 130, 170]:
        xStart = math.cos(angle*math.pi/180)*startOffset
        yStart = math.sin(angle*math.pi/180)*startOffset
        xEnd = math.cos(angle*math.pi/180)*length
        yEnd = math.sin(angle*math.pi/180)*length
        ax.arrow(xStart, yStart, xEnd, yEnd, head_width=250, head_length=500,
                 color='0.75', zorder = 1)


def main(args):
    if args.save == None:
        plt.ion() # Matplotlib interactive mode on.

    runControl = {'run': True,
                  'pause': False,
                  'nextLine': 0,
                  'step': False,
                  'goto': None}
    axisRange = [-20000, 15000, -500, 10000]
    projection = 'cartesian'
    scanCounter = []
    plotCaptions = []
    scans = []
    sensors = []

    if args.filename == None:
        for a in args.addresses:
            addr = a.split(":")
            addr = (addr[0], int(addr[1]))
            sensors.append(Pysick.Lidar(addr))
            sensors[-1].initialize(ScanFrequency = 25,
                              AngularResolution = 0.1667,
                              StartAngle = 5,
                              StopAngle = 175,
                              verbose = True)
            output = sensors[-1].singleScan()
            scans.append(Pysick.ScanData(output))
            scanCounter.append(0)
            plotCaptions.append("Distance scan [live], {} Hz, {:.4f}°, frame {{}}"
                                .format(scans[-1].ScanFrequency,
                                        scans[-1].Output16Bit[0].Steps))
        inputFile = None
    else:
        inputFile = [Pysick.ScanData(x)
                     if 'sSN' in x or 'sRA' in x
                     else x
                     for x in open(args.filename, 'r').readlines()]
        if args.goto != None:
            runControl['nextLine'] = args.goto
            runControl['pause'] = True
        if args.zoom != None:
            axisRange = args.zoom
        while type(inputFile[runControl['nextLine']]) != Pysick.ScanData:
            runControl['nextLine'] += 1

        serialNumbers = Counter([scan.SerialNumber for scan in
                        inputFile[runControl['nextLine']:]
                        if type(scan) == Pysick.ScanData])
        sensors = list(serialNumbers.keys())
        sensorCounts = list(serialNumbers.values())
        for i, sensor in enumerate(sensors):
            nextLine = runControl['nextLine']
            # Find the first scan from this sensor:
            scan = inputFile[nextLine]
            while type(scan) != Pysick.ScanData or scan.SerialNumber != sensor:
                nextLine += 1
                scan = inputFile[nextLine]
            scans.append(scan)
            scanCounter.append(0)
            runControl['nextLine'] += 1
            plotCaptions.append(("Distance scan [{}], {} Hz, "
                                 "{:.4f}°, frame {{}}/{}")
                                .format(
                                    os.path.basename(args.filename),
                                    scans[-1].ScanFrequency,
                                    scans[-1].Output16Bit[0].Steps,
                                    sensorCounts[i]))
    figures = []
    lines = []
    for i, sensor in enumerate(sensors):
        fig, line = Plot.plot(scans[i].Output16Bit[0],
                              projection = projection,
                              minimumDistance = 1000,
                              axisRange = axisRange,
                              caption = plotCaptions[i].format(scanCounter[i]))
        figures.append(fig)
        lines.append(line)
        drawVisualAids(fig, axisRange)
        fig.canvas.mpl_connect('key_press_event',
                               lambda event: keypressHandler(event, runControl))
        fig.canvas.mpl_connect('close_event',
                               lambda event: closeWindowHandler(event, runControl))

    if args.save != None:
        for i, fig in enumerate(figures):
            fig.savefig(args.save.replace('.', '{}.'.format(i)))
        return

    while (runControl['run'] and
           (inputFile is None or runControl['nextLine'] < len(inputFile))):
        if runControl['pause'] and not runControl['step']:
            with warnings.catch_warnings():
                # Suppress pointless deprecation warning:
                # http://stackoverflow.com/q/20003744/53345
                warnings.simplefilter("ignore") 
                plt.pause(0.01)
            continue
        if args.filename == None:
            for i, sensor in enumerate(sensors):
                scan = Pysick.ScanData(sensor.singleScan())
                if scan.ScanCounter == scans[i].ScanCounter:
                    # We just received a scan we have seen before. Ignore it.
                    continue
                scans[i] = scan
                scanCounter[i] += 1
        else:
            scan = inputFile[runControl['nextLine']]
            runControl['nextLine'] += 1
            # Right now we just print annotations:
            if type(scan) != Pysick.ScanData:
                print(scan.strip())
                continue
            sensor = sensors.index(scan.SerialNumber)
            scans[sensor] = scan
            scanCounter[sensor] += 1

        print(datetime.datetime.fromtimestamp(scan.TimeOfTransmission).strftime('%Y-%m-%d %H:%M:%S'))

        for i, scan in enumerate(scans):
            Plot.plot(scan.Output16Bit[0],
                      figures[i],
                      lines[i],
                      projection = projection,
                      minimumDistance = 10,
                      axisRange = axisRange,
                      caption = plotCaptions[i].format(scanCounter[i]))

        if runControl['step']:
            runControl['step'] = False

    if args.filename == None:
        for s in sensors:
            s.close()
    
    #plt.ioff() # Turn off interactive mode to keep program alive.
    #plt.show()
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Visualize lidar data either live or from a file.')
    parser.add_argument('-f',
                        '--filename',
                        nargs = '?',
                        metavar = 'input.dat',
                        default = None,
                        type = str,
                        help = ('The path to the datafile for visualization. '
                                'If no datafile is given, a live feed will '
                                'be shown.'))
    parser.add_argument('-a',
                        '--addresses',
                        nargs = '*',
                        metavar = '192.168.0.1:2112',
                        default = ['192.168.0.1:2112'],
                        type = str,
                        help = 'The IP-address(es) of the sensor(s).')
    parser.add_argument('-g',
                        '--goto',
                        type=int,
                        help= ('Frame number to jump to immediately,'
                               'if loading a file.'))
    parser.add_argument('-s',
                        '--save',
                        type=str,
                        help = ('Path to save plot of first frame to. '
                                'Use with -g to save a specific frame.'))
    parser.add_argument('-z',
                        '--zoom',
                        nargs=4,
                        type=int,
                        help='Axis limits to zoom to')
    args = parser.parse_args()
    main(args)
