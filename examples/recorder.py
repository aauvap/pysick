import re
import datetime
import sys
import os
import argparse
from pprint import pprint

import matplotlib.pyplot as plt

# Allow include
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))
import pysick.PysickLidar as Pysick
from kbhit import KBHit
    
def keypressHandler(event, runControl):
    if event.key == 'escape':
        runControl['run'] = False

def scanCallback(data, scanSettings, outfile):
    outfile.write("{}\n".format(data))
    scanSettings['numScans'] += 1
    if scanSettings['keyboard'].kbhit():
        c = scanSettings['keyboard'].getch()
        if ord(c) == 27: # ESC
            scanSettings['run'] = False
        elif c == ' ':
            scanSettings['truck'] = not scanSettings['truck']
            if scanSettings['truck']:
                print("TruckStart")
                outfile.write("TruckStart\n")
            else:
                print("TruckEnd")
                outfile.write("TruckEnd\n")
    if not scanSettings['run']:
        return False
    return True

def main(args):
    plt.ion() # Matplotlib interactive mode on.

    kb = KBHit() # Ready to receive keyboard input
    
    sensor = Pysick.Lidar(('192.168.0.1', 2112))
    sensor.initialize(ScanFrequency = 25,
                      AngularResolution = 0.1667,
                      StartAngle = 5,
                      StopAngle = 175,
                      verbose = True)

    outfile = open(datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S") +
                   '.dat', 'w')

    ## Continuous scan with callback
    scanSettings = {'run': True, 'numScans': 0, 'keyboard': kb, 'truck': False}
    print(sensor.startContinuousScan())
    sensor.readContinuousScan(lambda data: scanCallback(data,
                                                        scanSettings,
                                                        outfile))
    print(sensor.stopContinuousScan())
    
    sensor.close()
    outfile.close()

    print("Recorded {} scans.".format(scanSettings['numScans']))
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Visualize lidar data either live or from a file.')
    parser.add_argument('address',
                        nargs = '?',
                        metavar = '192.168.0.1:2112',
                        default = ['192.168.0.1:2112'],
                        type = str,
                        help = 'The IP-address of the sensor.')
    args = parser.parse_args()
    main(args)
