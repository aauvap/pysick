import argparse
import warnings
import math
import os
import sys
import inspect

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# Allow include
sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..'))
import pysick.PysickLidar as Pysick
from pysick.PysickPlot import Plot
    
def keypressHandler(event, runControl):
    if event.key == 'escape':
        runControl['run'] = False
    if event.key == ' ':
        runControl['pause'] = not runControl['pause']
    if event.key == 'right' and runControl['pause']:
        runControl['step'] = True
    if event.key == 'left' and runControl['pause']:
        runControl['step'] = True
        runControl['nextLine'] -= 2
    if event.key in ['1','2','3','4','5','6','7','8','9','0']:
        if runControl['goto'] == None:
            runControl['goto'] = int(event.key)
        else:
            runControl['goto'] *= 10
            runControl['goto'] += int(event.key)
    if ((event.key == 'g' or event.key == 'enter') and
            runControl['goto'] != None):
        runControl['step'] = True
        runControl['nextLine'] = runControl['goto']
        runControl['goto'] = None

def closeWindowHandler(event, runControl):
    # Lambdas disallow assignment, so we'll have a proper function doing this.
    runControl['run'] = False

def drawVisualAids(fig, axisRange):
    ax = fig.add_subplot(111)
    ax.plot([axisRange[0], axisRange[1]],
            [2500, 2500],
            '-', color = '0.75', lw = 2, zorder = 1)
    ax.plot([axisRange[0], axisRange[1]],
            [6000, 6000],
            '--', color = '0.75', lw = 1, zorder = 1)
    ax.plot([axisRange[0], axisRange[1]],
            [9500, 9500],
            '-', color = '0.75', lw = 2, zorder = 1)
    ax.add_patch(patches.Rectangle((-250, -500), 500, 500,
                                   facecolor="0.75", zorder = 1))
    startCoord = (0,0)
    startOffset = 500
    length = 800
    for angle in [10, 50, 90, 130, 170]:
        xStart = math.cos(angle*math.pi/180)*startOffset
        yStart = math.sin(angle*math.pi/180)*startOffset
        xEnd = math.cos(angle*math.pi/180)*length
        yEnd = math.sin(angle*math.pi/180)*length
        ax.arrow(xStart, yStart, xEnd, yEnd, head_width=250, head_length=500,
                 color='0.75', zorder = 1)


def main(args):
    if args.save == None:
        plt.ion() # Matplotlib interactive mode on.

    runControl = {'run': True,
                  'pause': False,
                  'nextLine': 0,
                  'step': False,
                  'goto': None}
    axisRange = [-20000, 15000, -500, 10000]
    projection = 'cartesian'

    if args.filename == None:
        sensor = Pysick.Lidar(('192.168.0.1', 2112))
        sensor.initialize(ScanFrequency = 25,
                          AngularResolution = 0.1667,
                          StartAngle = 5,
                          StopAngle = 175,
                          verbose = True)
    
        output = sensor.singleScan()
        data = Pysick.ScanData(output)
        scanCounter = 0
        plotCaption = "Distance scan [live], {} Hz, {:.4f}°, frame ".format(
            data.ScanFrequency,
            data.Output16Bit[0].Steps)
    else:
        inputFile = open(args.filename, 'r').readlines()
        if args.goto != None:
            runControl['nextLine'] = args.goto
            runControl['pause'] = True
        if args.zoom != None:
            axisRange = args.zoom
        data = Pysick.ScanData(inputFile[runControl['nextLine']])
        scanCounter = runControl['nextLine']
        runControl['nextLine'] += 1
        plotCaption = "Distance scan [{}], {} Hz, {:.4f}°, frame {{}}/{}".format(
            os.path.basename(args.filename),
            data.ScanFrequency,
            data.Output16Bit[0].Steps,
            len(inputFile))
        
    fig, line = Plot.plot(data.Output16Bit[0],
                              projection = projection,
                              minimumDistance = 1000,
                              axisRange = axisRange,
                              caption = plotCaption.format(scanCounter))

    drawVisualAids(fig, axisRange)

    if args.save != None:
        fig.savefig(args.save)
        return
        
    fig.canvas.mpl_connect('key_press_event',
                           lambda event: keypressHandler(event, runControl))
    fig.canvas.mpl_connect('close_event',
                           lambda event: closeWindowHandler(event, runControl))

    while runControl['run'] and runControl['nextLine'] < len(inputFile):
        if runControl['pause'] and not runControl['step']:
            with warnings.catch_warnings():
                # Suppress pointless deprecation warning: http://stackoverflow.com/q/20003744/53345
                warnings.simplefilter("ignore") 
                plt.pause(0.01)
            continue
        if args.filename == None:
            data = Pysick.ScanData(sensor.singleScan())
            scanCounter += 1
        else:
            data = inputFile[runControl['nextLine']]
            scanCounter = runControl['nextLine']
            runControl['nextLine'] += 1
            if "sSN" not in data: # Right now we just print annotations.
                print(data.strip())
                continue
            data = Pysick.ScanData(data)
        Plot.plot(data.Output16Bit[0],
                      fig,
                      line,
                      projection = projection,
                      minimumDistance = 1000,
                      axisRange = axisRange,
                      caption = plotCaption.format(scanCounter))

        if runControl['step']:
            runControl['step'] = False

    if args.filename == None:
        sensor.close()
    
    #plt.ioff() # Turn off interactive mode to keep program alive.
    #plt.show()
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Visualize lidar data either live or from a file.')
    parser.add_argument('filename',
                        nargs = '?',
                        metavar = 'input.dat',
                        default = None,
                        type = str,
                        help = ('The path to the datafile for visualization. '
                                'If no datafile is given, a live feed will '
                                'be shown.'))
    parser.add_argument('-g',
                        '--goto',
                        type=int,
                        help= ('Frame number to jump to immediately,'
                               'if loading a file.'))
    parser.add_argument('-s',
                        '--save',
                        type=str,
                        help = ('Path to save plot of first frame to. '
                                'Use with -g to save a specific frame.'))
    parser.add_argument('-z',
                        '--zoom',
                        nargs=4,
                        type=int,
                        help='Axis limits to zoom to')
    args = parser.parse_args()
    main(args)
