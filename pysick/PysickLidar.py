import math
import datetime
from collections import namedtuple
import socket
import sys
import struct
from enum import Enum

import numpy as np


class UserLevel(Enum):
    Maintenance = 2
    AuthorizedClient = 3
    Service = 4
    
class ScanConfig():
    def __init__(self, commandType, command, scanFrequency, value, angularResolution, startAngle, stopAngle):
        self.CommandType = commandType
        self.Command = command
        self.ScanFrequency = scanFrequency
        self.Value = value
        self.AngularResolution = angularResolution
        self.StartAngle = startAngle
        self.StopAngle = stopAngle
    def __str__(self):
        return ("ScanConfig: \n"
                "\tCommandType: {0.CommandType},\n"
                "\tCommand: {0.Command},\n"
                "\tScanFrequency: {0.ScanFrequency},\n"
                "\tValue: {0.Value},\n"
                "\tAngularResolution: {0.AngularResolution},\n"
                "\tStartAngle: {0.StartAngle}, # Do not trust! Use ScanData.StartAngle instead.\n"
                "\tStopAngle: {0.StopAngle} # Do not trust! Use ScanData.StopAngle instead.").format(self)
    @property
    def ScanFrequency(self):
        return self._ScanFrequency
    @ScanFrequency.setter
    def ScanFrequency(self, hexValue):
        self._ScanFrequency = int(hexValue, 16)/100
    
    @property
    def AngularResolution(self):
        return self._AngularResolution
    @AngularResolution.setter
    def AngularResolution(self, hexValue):
        self._AngularResolution = int(hexValue, 16)/10000 
    
    @property
    def StartAngle(self):
        return self._StartAngle
    @StartAngle.setter
    def StartAngle(self, hexValue):
        self._StartAngle = int(hexValue, 16)
        if self._StartAngle > 0x7FFFFFFF:
            self._StartAngle -= 0x100000000
        self._StartAngle /= 10000
        
    @property
    def StopAngle(self):
        return self._StopAngle
    @StopAngle.setter
    def StopAngle(self, hexValue):
        self._StopAngle = int(hexValue, 16)
        if self._StopAngle > 0x7FFFFFFF:
            self._StopAngle -= 0x100000000
        self._StopAngle /= 10000

class TimeData():
    def __init__(self, fields):
        self.Year = fields[0]
        self.Month = fields[1]
        self.Day = fields[2]
        self.Hour = fields[3]
        self.Minute = fields[4]
        self.Second = fields[5]
        self.uSeconds = fields[6]
        
    def __str__(self):
        return "{0.Year}-{0.Month}-{0.Day} {0.Hour}:{0.Minute}:{0.Second}.{0.uSeconds}".format(self)
    
    @property
    def Year(self):
        return self._Year
    @Year.setter
    def Year(self, hexValue):
        self._Year = int(hexValue, 16)
    
    @property
    def Day(self):
        return self._Day
    @Day.setter
    def Day(self, hexValue):
        self._Day = int(hexValue, 16)
    
    @property
    def Month(self):
        return self._Month
    @Month.setter
    def Month(self, hexValue):
        self._Month = int(hexValue, 16)
    
    @property
    def Hour(self):
        return self._Hour
    @Hour.setter
    def Hour(self, hexValue):
        self._Hour = int(hexValue, 16)
    
    @property
    def Minute(self):
        return self._Minute
    @Minute.setter
    def Minute(self, hexValue):
        self._Minute = int(hexValue, 16)
    
    @property
    def Second(self):
        return self._Second
    @Second.setter
    def Second(self, hexValue):
        self._Second = int(hexValue, 16)
    
    @property
    def uSeconds(self):
        return self._uSeconds
    @uSeconds.setter
    def uSeconds(self, hexValue):
        self._uSeconds = int(hexValue, 16)

class ScanOutput():
    def __init__(self, fields):
        self.Content = fields[0]
        self.ScaleFactor = fields[1]
        self.ScaleFactorOffset = fields[2]
        self.StartAngle = fields[3]
        self.Steps = fields[4]
        self.AmountOfData = fields[5]
        self.Data = fields[6:]
        self._CartesianData = None
        
    def __str__(self):
        return ("\t\tScaleFactor: {0.ScaleFactor}\n"
                "\t\tScaleFactorOffset: {0.ScaleFactorOffset}\n"
                "\t\tStartAngle: {0.StartAngle}\n"
                "\t\tSteps: {0.Steps}\n"
                "\t\tAmountOfData: {0.AmountOfData}\n"
                "\t\tData: {0.Data}").format(self)
    
    @property
    def ScaleFactor(self):
        return self._ScaleFactor
    @ScaleFactor.setter
    def ScaleFactor(self, hexValue):
        self._ScaleFactor = struct.unpack('!f', bytes.fromhex(hexValue))[0]
    
    @property
    def ScaleFactorOffset(self):
        return self._ScaleFactorOffset
    @ScaleFactorOffset.setter
    def ScaleFactorOffset(self, hexValue):
        self._ScaleFactorOffset = struct.unpack('!f', bytes.fromhex(hexValue))[0]
    
    @property
    def StartAngle(self):
        return self._StartAngle
    @StartAngle.setter
    def StartAngle(self, hexValue):
        self._StartAngle = int(hexValue, 16)
        if self._StartAngle > 0x7FFFFFFF:
            self._StartAngle -= 0x100000000
        self._StartAngle /= 10000
    
    @property
    def Steps(self):
        return self._Steps
    @Steps.setter
    def Steps(self, hexValue):
        # See Telegram Listing, page 20:
        self._Steps = 2.0/round(2.0/(int(hexValue, 16)/10000))

    @property
    def Angles(self):
        angles = np.arange(self.StartAngle, self.StartAngle+self.Steps*self.AmountOfData, self.Steps)
        angles *= np.pi/180 # Convert to radians
        return angles
    
    @property
    def AmountOfData(self):
        return self._AmountOfData
    @AmountOfData.setter
    def AmountOfData(self, hexValue):
        self._AmountOfData = int(hexValue, 16)
    
    @property
    def Data(self):
        return self._Data
    @Data.setter
    def Data(self, hexValues):
        self._Data = []
        for hexValue in hexValues:
            self._Data += [int(hexValue, 16)*self.ScaleFactor]

    @property
    def CartesianData(self):
        return self.CartesianData(0)

    def CartesianData(self, angleOffset):
        if self._CartesianData == None:
            angles = self.Angles+(angleOffset*(math.pi/180))
            distances = self.Data
            self._CartesianData = [(r*np.cos(a), r*np.sin(a))
                                   for r, a
                                   in zip(distances, angles)]
        return self._CartesianData



class ScanData():
    def __init__(self, telegram):
        fields = telegram.split()
        self.CommandType = fields[0]
        self.Command = fields[1]
        self.VersionNumber = int(fields[2])
        self.DeviceNumber = int(fields[3])
        self.SerialNumber = fields[4]
        self.DeviceStatus = fields[5] + fields[6] # Device status takes two fields
        self.TelegramCounter = fields[7]
        self.ScanCounter = fields[8]
        self.TimeSinceStartUp = fields[9]
        self.TimeOfTransmission = fields[10]
        self.StatusOfDigitalInputs = fields[11] + fields[12] # Takes two fields
        self.StatusOfDigitalOutputs = fields[13] + fields[14] # Takes two fields
        # Field #15 is reserved for vendor use
        self.ScanFrequency = fields[16]
        self.MeasurementFrequency = fields[17] # TODO: Understand this number!
        self.AmountOfEncoders = int(fields[18])
        nextField = 19 # From here, the number of fields depend on sensor configuration
        self.EncoderPosition = None
        self.EncoderSpeed = None
        if self.AmountOfEncoders > 0:
            self.EncoderPosition = []
            self.EncoderSpeed = []
            for i in range(self.AmountOfEncoders):
                self.EncoderPosition += [fields[nextField]] # Depends on AmountOfEncoders
                nextField += 1
                self.EncoderSpeed += [fields[nextField]] # Depends on AmountOfEncoders
                nextField += 1
        self.AmountOf16BitChannels = int(fields[nextField])
        nextField += 1
        self.Output16Bit = None
        if self.AmountOf16BitChannels > 0:
            self.Output16Bit = []
            for i in range(self.AmountOf16BitChannels):
                dataLength = int(fields[nextField+5], 16)
                self.Output16Bit += [ScanOutput(fields[nextField:nextField+6+dataLength])]
                nextField += 6+dataLength
        self.AmountOf8BitChannels = int(fields[nextField])
        nextField += 1
        self.Output8Bit = None
        if self.AmountOf8BitChannels > 0:
            self.Output8Bit = []
            for i in range(self.AmountOf8BitChannels):
                dataLength = int(fields[nextField+5], 16)
                self.Output8Bit += [ScanOutput(fields[nextField:nextField+6+dataLength])]
                nextField += 6+dataLength
        self.PositionDataPresent = (fields[nextField] == "1") # Never present, do not try to access subfields.
        nextField += 1
        self.DeviceNamePresent = (fields[nextField] == "1")
        nextField += 1
        self.DeviceNameLength = None
        self.DeviceName = None
        if self.DeviceNamePresent:
            self.DeviceNameLength = fields[nextField]
            nextField += 1
            self.DeviceName = fields[nextField]
            nextField += 1
        self.CommentPresent = (fields[nextField] == "1") # Never present, do not try to access subfields.
        nextField += 1
        self.TimePresent = (fields[nextField] == "1")
        nextField += 1
        self.Time = None
        if self.TimePresent > 0:
            self.Time = TimeData(fields[nextField:nextField+7])
            nextField += 7
        self.EventInfoPresent = (fields[nextField] == "1")  # Never present, do not try to access subfields.
    def __str__(self):
        string = ("ScanData: \n"
                  "\tCommandType: {0.CommandType},\n"
                  "\tCommand: {0.Command},\n"
                  "\tVersionNumber: {0.VersionNumber},\n"
                  "\tDeviceNumber: {0.DeviceNumber},\n"
                  "\tSerialNumber: {0.SerialNumber},\n"
                  "\tDeviceStatus: {0.DeviceStatus},\n"
                  "\tTelegramCounter: {0.TelegramCounter},\n"
                  "\tScanCounter: {0.ScanCounter},\n"
                  "\tTimeSinceStartUp: {0.TimeSinceStartUp},\n"
                  "\tTimeOfTransmission: {0.TimeOfTransmission},\n"
                  "\tStatusOfDigitalInputs: {0.StatusOfDigitalInputs},\n"
                  "\tStatusOfDigitalOutputs: {0.StatusOfDigitalOutputs},\n"
                  "\tScanFrequency: {0.ScanFrequency},\n"
                  "\tMeasurementFrequency: {0.MeasurementFrequency},\n"
                  "\tAmountOfEncoders: {0.AmountOfEncoders},\n"
                  "\tEncoderPosition: {0.EncoderPosition},\n"
                  "\tEncoderSpeed: {0.EncoderSpeed},\n"
                  "\tAmountOf16BitChannels: {0.AmountOf16BitChannels},\n"
                  "\tOutput16Bit: ").format(self)
        if self.Output16Bit != None:
            string += "["
            for output in self.Output16Bit:
                string += "\n{}".format(output)
            string += "]"
        else:
            string += "None"
        string += (",\n"
                   "\tAmountOf8BitChannels: {0.AmountOf8BitChannels},\n"
                   "\tOutput8Bit: ").format(self)
        if self.Output8Bit != None:
            string += "["
            for output in self.Output8Bit:
                string += "\n{}".format(output)
            string += "]"
        else:
            string += "None"
        string += (",\n"
                   "\tPositionDataPresent: {0.PositionDataPresent},\n"
                   "\tDeviceNamePresent: {0.DeviceNamePresent},\n"
                   "\tDeviceNameLength: {0.DeviceNameLength},\n"
                   "\tDeviceName: {0.DeviceName},\n"
                   "\tCommentPresent: {0.CommentPresent},\n"
                   "\tTimePresent: {0.TimePresent},\n"
                   "\tTime: {0.Time},\n"
                   "\tEventInfoPresent: {0.EventInfoPresent}").format(self)
        return string
                
                
    @property
    def TelegramCounter(self):
        return self._TelegramCounter    
    @TelegramCounter.setter
    def TelegramCounter(self, hexValue):
        self._TelegramCounter = int(hexValue, 16)

    @property
    def ScanCounter(self):
        return self._ScanCounter    
    @ScanCounter.setter
    def ScanCounter(self, hexValue):
        self._ScanCounter = int(hexValue, 16)

    @property
    def TimeSinceStartUp(self):
        return self._TimeSinceStartUp    
    @TimeSinceStartUp.setter
    def TimeSinceStartUp(self, hexValue):
        self._TimeSinceStartUp = int(hexValue, 16)

    @property
    def TimeOfTransmission(self):
        return self._TimeOfTransmission    
    @TimeOfTransmission.setter
    def TimeOfTransmission(self, hexValue):
        self._TimeOfTransmission = int(hexValue, 16)

    @property
    def ScanFrequency(self):
        return self._ScanFrequency
    @ScanFrequency.setter
    def ScanFrequency(self, hexValue):
        self._ScanFrequency = int(hexValue, 16)/100

    @property
    def MeasurementFrequency(self):
        return self._MeasurementFrequency
    @MeasurementFrequency.setter
    def MeasurementFrequency(self, hexValue):
        self._MeasurementFrequency = int(hexValue, 16)
'''
sRN: Read by name
sWN: Write by name
sMN: Method
sEN: Event
'''
class Lidar:
    def __init__(self, address):
        self.sensorAddr = address
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect(self.sensorAddr)
        
    def query(self, queryText):
        self.sock.sendall((chr(2) + queryText + chr(3)).encode('ascii'))
        reply = ''
        while True:
            data = self.sock.recv(16)
            reply += data.decode()
            if chr(3).encode('ascii') in data:
                break
        return reply[1:-1] # Removing the STX/ETX control characters.
    
    def close(self):
        self.sock.close()
        print('Closed connection')
        
    def identify(self):
        return self.query('sRN DeviceIdent')
    
    def login(self, level = UserLevel.AuthorizedClient):
        # Logging in as Authorized Client
        password = {
            UserLevel.Maintenance: 'B21ACE26',
            UserLevel.AuthorizedClient: 'F4724744',
            UserLevel.Service: '81BE23AA'
        }
        return self.query('sMN SetAccessMode {:02d} {}'.format(level.value, password[level]))
    
    def readScanConfig(self):
        return ScanConfig(*self.query('sRN LMPscancfg').split())

    def writeScanConfig(self, ScanFrequency = 50, AngularResolution = 0.5):
        assert(ScanFrequency in [25, 35, 50, 75, 100])
        assert(AngularResolution in [0.1667, 0.25, 0.333, 0.5, 0.667, 1])
        # Start and end angle cannot be set with this command, so we just use dummy values.
        # See writeOutputRange() instead.
        return self.query('sMN mLMPsetscancfg {:+.0f} +1 {:+.0f} -50000 +1850000'
                          .format(ScanFrequency*100,
                                  AngularResolution*10000))
    def writeOutputRange(self, StartAngle = -5, StopAngle = 185):
        assert(-5 <= StartAngle <= 185)
        assert(-5 <= StopAngle <= 185)
        # Angle resolution cannot be set with this command, so we just use a dummy value.
        # See writeScanConfig() instead.
        return self.query('sWN LMPoutputRange 1 +5000 {:+.0f} {:+.0f}'
                          .format(StartAngle*10000,
                                  StopAngle*10000))

    def setTime(self):
        t = datetime.datetime.now()
        return self.query('sMN LSPsetdatetime {:+d} {:+d} {:+d} {:+d} {:+d} {:+d} {:+d}'.
                          format(t.year,
                                 t.month,
                                 t.day,
                                 t.hour,
                                 t.minute,
                                 t.second,
                                 t.microsecond))
    
    def startMeasurement(self):
        return self.query('sMN LMCstartmeas')
    
    def run(self):
        return self.query('sMN Run')

    ''' Alias for run() '''    
    def logout(self):
        return self.run()
    
    def queryMeasuringStatus(self):
        while True:
            reply = self.query('sRN STlms')
            if reply.split(' ')[2] == '7':
                return reply
    
    def singleScan(self):
        return self.query('sRN LMDscandata')

    def startContinuousScan(self):
        return self.query('sEN LMDscandata 1')

    def readContinuousScan(self, callback):
        while True:
            reply = ''
            while True:
                data = self.sock.recv(16)
                reply += data.decode()
                if chr(3).encode('ascii') in data:
                    break
            if not callback(reply[1:-1]): # Removing the STX/ETX control characters.
                return
        
    def stopContinuousScan(self):
        return self.query('sEN LMDscandata 0')

    def initialize(self,
                   ScanFrequency = 50,
                   AngularResolution = 0.5,
                   StartAngle = -5,
                   StopAngle = 185,
                   verbose = False):
        Command = namedtuple('Command', ['function', 'parameters'])
        Command.__new__.__defaults__ = ([],)
        commandList = [
            Command(self.identify),
            Command(self.login),
            Command(self.setTime),
            Command(self.writeScanConfig, [ScanFrequency, AngularResolution]),
            Command(self.writeOutputRange, [StartAngle, StopAngle]),
            Command(self.readScanConfig),
            Command(self.startMeasurement),
            Command(self.run),
            Command(self.queryMeasuringStatus)
        ]
        results = []
        for c in commandList:
            results.append(c.function(*c.parameters))
            if verbose:
                print(results[-1])
        return results
