import warnings
import numpy as np
import matplotlib.pyplot as plt

class Plot:
    @staticmethod
    def plot(scanOutput,
             fig = None,
             line = None,
             projection = 'polar',
             minimumDistance = None,
             redraw = False,
             axisRange = None,
             caption = None,
             scatter = False):
        angles = scanOutput.Angles
        
        distances = scanOutput.Data
        if minimumDistance != None:
            distances, angles = list(zip(*[(r,a)
                                           for r, a
                                           in zip(distances, angles)
                                           if r > minimumDistance]))

        if projection == 'polar':
            data = [angles, distances]
        else:
            projection = None
            data = list(zip(*[(r*np.cos(a), r*np.sin(a))
                              for r, a
                              in zip(distances, angles)]))
            
            # scanData.CartesianData is not used because
            # minimumDistance filtering done above does not reflect in
            # the original scanOutput:
            # data = list(zip(*scanOutput.CartesianData))
        if fig == None:
            fig = plt.figure(figsize=(12, 5))
            
        ax = fig.add_subplot(111, projection=projection)
        
        if line == None or redraw:
            ax.clear()
            ax.set_xlabel("Distance [mm]")
            ax.set_ylabel("Distance [mm]")
            if scatter:
                line, = ax.plot(*data, linestyle='', marker = '.')
            else:
                line, = ax.plot(*data)
        else:
            line.set_xdata(data[0])
            line.set_ydata(data[1])


        if projection != 'polar':       
            ax.axis('equal')
            if axisRange != None:
                ax.axis(axisRange)
                ax.set_autoscale_on(False)

        if caption != None:
            ax.set_title(caption, y = 1.05)
            
        with warnings.catch_warnings():
            # Suppress pointless deprecation warning:
            # http://stackoverflow.com/q/20003744/53345
            warnings.simplefilter("ignore") 
            plt.pause(0.01)
        return (fig, line)
